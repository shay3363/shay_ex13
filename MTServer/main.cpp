#include "Server.h"
#include <iostream>
#include <exception>
#define MY_PORT 1234

int main(void)
{
	WSADATA wsaData;

	//activate ws2_32.lib
	int res = WSAStartup(MAKEWORD(2, 0), &wsaData);
	if (res != 0) {
		std::cout << "Error WSAStartup" << std::endl;
		return -201;
	}
	try
	{
		Server myServer = Server();
		myServer.serve(MY_PORT);
	}
	catch (...)
	{
		std::cout << "fail" << std::endl;
		system("pause");
		return 1;
	}
	system("pause");
	return 0;
}