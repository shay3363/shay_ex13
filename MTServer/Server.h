#pragma once
#pragma comment(lib, "ws2_32.lib")
#include <mutex>
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include <iostream>
#include <string>
#include <queue>
#include <vector>
class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	void accept();
	void clientHandler(SOCKET clientSocket);
	std::vector<std::string> _users;
	std::queue<std::string> _msg;
	SOCKET _serverSocket;
};