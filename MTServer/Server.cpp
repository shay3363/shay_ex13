#include "Server.h"
#include <thread>
#include "Helper.h"

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread clientThread(&Server::clientHandler,this,std::ref(client_socket));
	clientThread.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::cout << "client Connected" << std::endl;
	try
	{
		int typeCode = Helper::getMessageTypeCode(clientSocket);
		while (typeCode != 0 && (typeCode == MT_CLIENT_LOG_IN || typeCode == MT_CLIENT_UPDATE || typeCode == MT_CLIENT_FINISH))
		{
			//switch (typeCode)
			//{
			//case MT_CLIENT_LOG_IN:
			//	this->_users.push_back(Helper::getStringPartFromSocket(clientSocket, Helper::getIntPartFromSocket(clientSocket, 2)));
			//	Helper::sendData(clientSocket, "101");
			//	break;
			//case MT_CLIENT_UPDATE:
			//	break;
			//case MT_CLIENT_FINISH:
			//	break;
			//}
			std::cout << typeCode << '\n';
			typeCode = Helper::getMessageTypeCode(clientSocket);
			std::cout << typeCode << '\n';
		}
		//Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}


}